Description
----------
Ball catching experiment.

Technology stack - _OpenCV_, _pygame_.

![Alt text](/img/output.gif?raw=true "Experiment Example")

Installation
----------

Clone project

    git clone https://gitlab.com/kozhemiakink/catch-ball.git
    cd catch-ball

Install dependencies

    pip3 install -r requirements.txt

Run project

    python3 main.py

Settings
----------

- ``G``        - gravity acceleration (pixels / sec^2);
- ``speed0``   - initial ball velocity (pixels / sec);
- ``angle0``   - initial direction angle (degrees);

- ``window_name`` - experiment window name;
- ``fps`` - frames per second;
- ``width`` - window width in pixels;
- ``height`` - window height in pixels;
- ``background_color`` - experiment background color;

- ``ball_color`` - ball color;
- ``ball_radius`` - ball radius in pixels;
- ``ball_center0`` - initial ball location;

- ``platform_radius`` - platform radius in pixels;
- ``platform_center0`` - initial platform location;

- ``line1_x`` - left vertical gray line coordinate;
- ``line2_x`` - right vertical gray line coordinate.
