import sys
import pygame

import settings
from src import Enviroment


pygame.init()

clock = pygame.time.Clock()

screen = pygame.display.set_mode(
    (settings.width, settings.height)
)
pygame.display.set_caption(settings.window_name)

enviroment = Enviroment(settings)
# Game loop.
tick = 0
while True:
    screen.fill(settings.background_color)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit(0)

    enviroment.draw(screen, tick / settings.fps)

    pygame.display.flip()
    tick += 1
    clock.tick(settings.fps)

