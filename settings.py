# physics settings
# G = 9.80665  # gravity acceleration (pixels / sec^2) 
G = 25  # gravity acceleration (pixels / sec^2) 
speed0 = 150 # pixels / sec
angle0 = 35  # (degrees)

# window settings
window_name = 'Catch Ball'
fps = 10
width, height = 640, 480
background_color = (240, 240, 240)  # rgb

# ball settings
ball_color = (255, 0, 0)  # rgb
ball_radius = 10
ball_center0 = (ball_radius, 280)

# platform settings
platform_radius = 2 * ball_radius
platform_center0 = (width - ball_radius, int(height / 2))

# enviromen settings
line1_x = 100
line2_x = 400
expirements_interval = 3  # (sec)
