import random
import pygame

from .figures import Ball, Platform


class Enviroment:
    def __init__(self, settings):
        pygame.font.init()

        self.font = pygame.font.SysFont('Comic Sans MS', 30)

        self.ball = Ball(
            settings.ball_center0,
            settings.ball_radius,
            settings.ball_color,
            settings.angle0 + random.randint(0, 30) - 15,
            settings.speed0,
            settings.G
        )

        self.platform = Platform(
            settings.platform_center0,
            settings.platform_radius,
            settings.line1_x,
            settings.line2_x,
        )

        self.line1_x = settings.line1_x
        self.line2_x = settings.line2_x
        self.expirements_interval = settings.expirements_interval

        self.stopped = False
        self.stopped_time_moment = None
        self.result = None
        self.result_color = None

    def draw(self, screen, time_moment=0):
        if self.stopped:
            textsurface = self.font.render(self.result, False, self.result_color)
            screen.blit(
                textsurface,
                (
                    int(screen.get_width() / 2 - textsurface.get_width()),
                    int(screen.get_height() / 2 - textsurface.get_height())
                )
            )
            time_moment = self.stopped_time_moment

        pygame.draw.line(
            screen,
            (125, 125, 125), 
            (self.line1_x - 4, 0), 
            (self.line1_x - 4, screen.get_height()),
            4
        )

        pygame.draw.line(
            screen,
            (125, 125, 125), 
            (self.line2_x, 0), 
            (self.line2_x, screen.get_height()),
            4
        )

        rb = self.ball.radius
        xb, yb = self.ball.draw(screen, time_moment)
        xp, yp = self.platform.draw(screen, time_moment)
        yp1, yp2 = yp - self.platform.radius, yp + self.platform.radius

        # check if Game Over
        if not self.stopped:
            if xb + rb >= xp and yp1 <= yb <= yp2:
                self.stopped = True
                self.stopped_time_moment = time_moment
                self.result = 'Success'
                self.result_color = (0, 255, 0)
            elif (xb - rb < 0 or
                xb + rb > screen.get_width() or
                yb + rb > screen.get_height()):
                self.stopped = True
                self.stopped_time_moment = time_moment
                self.result = 'Failer'
                self.result_color = (255, 0, 0)
