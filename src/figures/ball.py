import math
import pygame


class Ball:
    def __init__(self, center, radius, color,
            angle0=0, speed0=0, G=9.80665):
        self.center0 = center
        self.radius = radius
        self.color = color
        self.angle0 = math.radians(-angle0)
        self.speed0 = speed0
        self.G = G

        # print(f'Vx= {speed0 * math.cos(self.angle0)}')

        self.last_position = self.center0

    def draw(self, screen, time_moment=0):
        x = self.speed0 * time_moment * math.cos(self.angle0)
        # !!!! we are using this formula because of inverted screen height coordinate
        y = (self.G * time_moment * time_moment / 2 +
                self.speed0 * time_moment * math.sin(self.angle0))
        x0, y0 = self.center0
        center = (x + x0, y + y0)

        pygame.draw.circle(screen, self.color, center, self.radius)
        self.last_position = center
        return center
