import cv2
import math
import pygame
import scipy.linalg as linalg
import numpy as np


class Platform:
    def __init__(self, center, radius, x1, x2):
        self.center = center
        self.radius = radius
        self.x1, self.x2 = x1, x2

        self.measurements = np.empty((0, 2))
        self.abc = []
        # self.meeting_time = None
        # self.speed_x = None
        # self.speed_y1 = None
        # self.acceleration = None

    def draw(self, screen, time_moment=0):
        subsurface = screen.subsurface((self.x1, 0, self.x2 - self.x1, screen.get_height()))
        img_rgb = pygame.surfarray.array3d(subsurface).transpose((1, 0, 2))

        circle = self.find_circle(img_rgb)
        if circle is not None:
            (circle_x, circle_y), radius = circle
            pygame.draw.circle(screen, (0, 0, 0), (circle_x + self.x1, circle_y), radius, width=2)

            measurement = np.array((circle_x + radius, circle_y)).reshape(1, 2)
            self.measurements = np.concatenate(
                (self.measurements, measurement),
                axis=0
            )

            if len(self.measurements) > 2:
                middle = len(self.measurements) // 2
                x1, y1 = self.measurements[0]
                x2, y2 = self.measurements[middle]
                x3, y3 = self.measurements[-1]

                x1, x2, x3 = x1 + self.x1, x2 + self.x1, x3 + self.x1

                coeff_matrix = np.matrix([
                    [x1 ** 2, x1, 1],
                    [x2 ** 2, x2, 1],
                    [x3 ** 2, x3, 1]
                ])
                res_matrix = np.matrix([[y1], [y2], [y3]])
                self.abc = linalg.solve(coeff_matrix, res_matrix).flatten()

                x = self.center[0]
                new_y = np.sum(self.abc * (x ** 2, x, 1))
                self.center = (x, np.clip(new_y, self.radius, screen.get_height() - self.radius))


        x, y = self.center
        pygame.draw.line(
            screen,
            (0, 0, 0), 
            (x, y - self.radius), 
            (x, y + self.radius),
            3
        )
        for circle_x, circle_y in self.measurements:
            pygame.draw.circle(screen, (0, 0, 0), (circle_x + self.x1, circle_y), 1)

        # if len(self.measurements) > 2:
        #     xes = np.arange(100, 640, 10)
        #     yes = xes ** 2 * self.abc[0] + xes * self.abc[1] + self.abc[2]
        #     for i in range(1, len(xes)):
        #         pygame.draw.line(
        #             screen,
        #             (0, 255, 125), 
        #             (xes[i-1], yes[i-1]), 
        #             (xes[i], yes[i]),
        #             2
        #         )

        return self.center

    @staticmethod
    def find_circle(img_rgb, E_peri=0.1, E_area=0.1):
        gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
        canny = cv2.Canny(gray, 100, 200)

        cnts, _ = cv2.findContours(
            canny,
            cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE
        )
        for c in cnts:
            peri = cv2.arcLength(c, True)
            area = cv2.contourArea(c)

            center, radius = cv2.minEnclosingCircle(c)
            area0 = math.pi * radius * radius
            peri0 = 2 * math.pi * radius

            if (abs(1 - peri / peri0) < E_peri and
                abs(1 - area / area0) < E_area):
                # return (int(center[0]), int(center[1])), int(radius)
                return center, radius

        return None
